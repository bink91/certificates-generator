from Crypto.PublicKey import RSA
from datetime import datetime, timedelta
import uuid
import hashlib
import base64
import sys

CERTIFICATE_VERSION = "1.1"


class CertificateData:
    version_number = None
    serial_number = None
    timestamp = None
    expiration_date = None
    country_code = None
    is_personal = None
    unit_name = None

    def serialize(self):
        data = {
            "vn": self.version_number,
            "sn": self.serial_number,
            "ts": self.timestamp,
            "ed": self.expiration_date,
            "cc": self.country_code,
            "ip": self.is_personal,
            "un": self.unit_name
        }

        return "|".join(["{0}:{1}".format(key, value) for key, value in data.items()])

    @staticmethod
    def unserialize(data):
        certificate_data = CertificateData()

        chunks = data.split("|")
        for pair in chunks:
            key = pair[0:2]
            value = pair[3:]

            if key == "vn":
                certificate_data.version_number = value

            if key == "sn":
                certificate_data.serial_number = value

            if key == "ts":
                certificate_data.timestamp = value

            if key == "ed":
                certificate_data.expiration_date = value

            if key == "cc":
                certificate_data.country_code = value

            if key == "ip":
                certificate_data.is_personal = bool(value)

            if key == "un":
                certificate_data.unit_name = value

        return certificate_data

    def __repr__(self):
        return "vn: {vn}\nsn: {sn}\nts: {ts}\ned: {ed}\ncc: {cc}\nip: {ip}\nun: {un}".format(vn=self.version_number,
                                                                                             sn=self.serial_number,
                                                                                             ts=self.timestamp,
                                                                                             ed=self.expiration_date,
                                                                                             cc=self.country_code,
                                                                                             ip=self.is_personal,
                                                                                             un=self.unit_name)


def between(left, right, s):
    before, _, a = s.partition(left)
    a, _, after = a.partition(right)
    return a


def verify_cert():
    with open("certificate.pem", "r") as file:
        certificate = file.read()
        certificate_body = between("-----BEGIN CERTIFICATE-----", "-----END CERTIFICATE-----", certificate)
        certificate_body = base64.b64decode(certificate_body).decode("utf-8")

        data = certificate_body.split("-----BEGIN FINGERPRINT-----")[0]
        finger_print = hashlib.sha1(str(data).encode("utf-8"))
        finger_print = str(finger_print.hexdigest()).strip()
        saved_fingerprint = between("-----BEGIN FINGERPRINT-----", "-----END FINGERPRINT-----",
                                    certificate_body).strip()

        if finger_print == saved_fingerprint:
            print("Status: Certyfikat został zweryfikowany")
        else:
            print("Status: Nie udało się zweryfikować certyfikatu.")


def print_cert_data():
    with open("certificate.pem", "r") as file:
        certificate = file.read()
        certificate_body = between("-----BEGIN CERTIFICATE-----", "-----END CERTIFICATE-----", certificate)
        certificate_body = base64.b64decode(certificate_body).decode("utf-8")

        personal_data = between("-----BEGIN PERSONAL DATA-----", "-----END PERSONAL DATA-----", certificate_body).strip()
        print(CertificateData.unserialize(personal_data))


def generate_cert(certificate_data):
    key = RSA.generate(2048)

    with open("private.key", "wb") as file:
        file.write(key.exportKey("PEM"))

    certificate_body = key.publickey().exportKey("PEM")
    certificate_body += bytes("\n-----BEGIN PERSONAL DATA-----\n", "utf-8")
    certificate_body += bytes(certificate_data.serialize(), "utf-8")
    certificate_body += bytes("\n-----END PERSONAL DATA-----\n", "utf-8")

    finger_print = hashlib.sha1(certificate_body)

    certificate_body += bytes("-----BEGIN FINGERPRINT-----\n", "utf-8")
    certificate_body += bytes(finger_print.hexdigest(), "utf-8")
    certificate_body += bytes("\n-----END FINGERPRINT-----\n", "utf-8")

    certificate = bytes("-----BEGIN CERTIFICATE-----\n", "utf-8")
    certificate += base64.b64encode(certificate_body)
    certificate += bytes("\n-----END CERTIFICATE-----", "utf-8")

    with open("certificate.pem", "wb") as file:
        file.write(certificate)


if __name__ == "__main__":
    if len(sys.argv) == 1:
        print("Brak argumentów")
    else:
        if sys.argv[1] == "cert:generate":
            print("\tGenerowanie Certyfikatu\n\n")

            country_code = ""
            while len(country_code) is not 2:
                country_code = input("Kod kraju (np. PL): ")
                country_code = str(country_code).upper()

            is_personal = None
            while is_personal not in ['y', 'n']:
                is_personal = input("Certyfikat prywatny (y/n): ")
                is_personal = str(is_personal).lower()

            name = input("Nazwa: ")

            certificate_data = CertificateData()
            certificate_data.version_number = CERTIFICATE_VERSION
            certificate_data.serial_number = uuid.uuid4()

            today_date = datetime.today()
            certificate_data.timestamp = today_date.timestamp()
            certificate_data.expiration_date = (today_date + timedelta(366)).timestamp()
            certificate_data.country_code = country_code
            certificate_data.is_personal = is_personal == "y"
            certificate_data.unit_name = name

            cmd = None
            while cmd not in ['y', 'n']:
                cmd = input("Wygenerować certyfikat (y/n): ")
                cmd = str(cmd).lower()

            if cmd == 'y':
                generate_cert(certificate_data)
                print("Ceryfikat został wygenerowany.")
            else:
                print("Nie wygenerowano certyfikartu.")

        elif sys.argv[1] == "cert:verify":
            verify_cert()
        elif sys.argv[1] == "cert:print":
            print_cert_data()
